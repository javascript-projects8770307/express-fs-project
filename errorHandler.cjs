function errorHandler(err, req, res, next) {

    console.error(err);

    if(!err.syscall){
        res.status(400).json(err).end();
    }
    else if (err.syscall === 'mkdir') {
        res.status(500).json({
            message: "Error Creating Directory!!",
            code: 500
        }).end();
    }
    else if (err.syscall === 'unlink') {
        res.status(500).json({
            message: "Error Deleting Files check if file exists!!",
            code: 500
        }).end();
    }
    else if(err.syscall === 'writeFile'){
        res.status(500).json({
            message: "Error Creating File!!",
            code: 500
        }).end();
    }
    else {
        res.status(500).json({
            message: "Internal Server Error",
            code: 500
        })
    }
}

module.exports = errorHandler;