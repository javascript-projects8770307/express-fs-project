const fs = require('fs');
const path = require('path');


function createDirectory(folderName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.resolve(folderName), { recursive: true }, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                resolve(`${folderName} created Successfully`);
            }
        })
    })
}

function createFiles(folder, listOfFiles) {
    return listOfFiles.map(index => {
        const fileName = index + ".json";
        return new Promise((resolve, reject) => {
            fs.writeFile(path.resolve(folder, fileName), '', (err) => {
                if (err) {
                    console.error(`Error creating ${fileName}`, err);
                    reject(err);
                } else {
                    resolve(`${fileName}`);
                }
            })
        })
    })
}



function deleteFilesInADirectory(folderName, fileNames) {
    return fileNames.map(index => {
        const fileName = index;
        return new Promise((resolve, reject) => {
            fs.unlink(path.resolve(folderName, fileName), (err) => {
                if (err) {
                    console.error(`Error deleting ${fileName}`, err);
                    reject(err);
                } else {
                    resolve(`${fileName}`);
                }
            });
        });
    });
}

module.exports = {
    createDirectory,
    createFiles,
    deleteFilesInADirectory
};