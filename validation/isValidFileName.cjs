const isValidFileName = (fileName) => {
    if (fileName.length < 1) {
        return false;
    } else {
        
        const fileNameCharacterArray = fileName.split("");
        if (fileName[0] === '-' || fileName[0] === '.') {
            console.log(fileName[0])
            return false;
        } else {

            return fileNameCharacterArray.map(char => {
                code = char.charCodeAt(0);
                if (!(code > 47 && code < 58) && // numeric (0-9)
                    !(code > 64 && code < 91) && // upper alpha (A-Z)
                    !(code > 96 && code < 123) && // lower alpha (a-z)
                    !('_' === char) &&//
                    !('-' === char) &&
                    !('.' === char)) {
                    return false;
                } else {
                    return true;
                }

            }).reduce((acc, currentValue) => {
                return acc && currentValue;
            }, true);
        }
    }
}

module.exports = isValidFileName;