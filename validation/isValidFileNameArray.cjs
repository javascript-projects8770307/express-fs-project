const isValidFileName = require('./isValidFileName.cjs');

const isValidFileNameArray = (fileNameArray) => {
    return fileNameArray.map(file => {
        if (typeof file !== 'string') {
            return false;
        } else {
            return isValidFileName(file);
        }
    }).reduce((acc, currentValue) => {
        return acc && currentValue;
    }, true);
}

module.exports = isValidFileNameArray;
