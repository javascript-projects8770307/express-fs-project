const isValidDirectoryName = (directoryName) => {
    if (directoryName.length < 1) {
        return false;

    } else {
        const directoryNameCharacterArray = directoryName.split("");

        if (directoryName[0] === '_' || directoryName[0] === '-') {
            return false;

        } else {
            return directoryNameCharacterArray.map(char => {
                let code = char.charCodeAt(0);
                if (!(code > 47 && code < 58) && // numeric (0-9)
                    !(code > 64 && code < 91) && // upper alpha (A-Z)
                    !(code > 96 && code < 123) && // lower alpha (a-z)
                    !('_' === char) &&//
                    !('-' === char)&&
                    !(' ' === char)) {
                    return false;

                } else {
                    return true;
                }

            }).reduce((acc, currentValue) => {
                return acc && currentValue;
            }, true);
        }
    }
}

module.exports = isValidDirectoryName;