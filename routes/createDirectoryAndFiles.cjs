const { createRandomFiles, deleteFiles } = require('../controller/createDirectoryAndFiles.cjs');

const express = require('express');
const router = express.Router();


//create random files
router.post('/create', createRandomFiles);
router.delete('/delete', deleteFiles);


module.exports = router;



