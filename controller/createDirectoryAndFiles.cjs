const { createDirectory, createFiles, deleteFilesInADirectory } = require("../utils/fileUtil.cjs");
const isValidDirectoryName = require('../validation/isValidDirectoryName.cjs');
const isValidFileNameArray = require("../validation/isValidFileNameArray.cjs");

function createRandomFiles(req, res, next) {
    try {
        if (!req.body.directoryName || !req.body.randomFiles) {
            next({
                message: "Object must contain directoryName and randomFiles as key in the body should not contain empty string",
                code: 400
            });

        } else {
            let directory = req.body.directoryName;
            if (!Array.isArray(req.body.randomFiles) || !isValidFileNameArray(req.body.randomFiles)) {
                next({
                    message: "File must be in an array shoud be a string only should contain only -, _ and alphaNumeric Characters should not start with _ and -",
                    code: 400
                });

            } else if (!isValidDirectoryName(directory)) {
                next({
                    message: "Directory must be a string should contain only -, _ and alphaNumeric Characters should not start with _ , - and  <space>",
                    code: 400
                });

            } else {
                createDirectory(directory).then(data => {
                    return Promise.all(createFiles(directory, req.body.randomFiles));
                }).then(data => {
                    res.status(200);
                    res.json({
                        message: "Directory of files has been created successfully",
                        code: 200
                    }).end();
                }).catch(err => {
                    next(err);
                })
            }
        }

    } catch (err) {
        next(err);
    }
}

function deleteFiles(req, res, next) {
    try {
        if (!req.body.directoryName || !req.body.randomFiles) {
            next({
                message: "Object must contain directoryName and randomFiles as key in the body",
                code: 400
            });

        } else {

            let directory = req.body.directoryName;
            if (!Array.isArray(req.body.randomFiles) || !isValidFileNameArray(req.body.randomFiles)) {
                next({
                    message: "File must be in an array shoud be a string, Try using different name",
                    code: 400
                });

            } else if (!isValidDirectoryName(directory)) {

                next({
                    message: "Directory must be a string should contain only alpha numeric character",
                    code: 400
                });

            } else {

                Promise.all(deleteFilesInADirectory(directory, req.body.randomFiles)).then(data => {
                    res.status(200);
                    res.json({
                        message: "Files has been deleted successfully",
                        code: 200
                    }).end();
                }).catch(err => {
                    next(err);
                })
            }
        }
    }
    catch (err) {
        next(err);
    }
}

module.exports = {
    createRandomFiles,
    deleteFiles
};