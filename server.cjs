const express = require('express');
const path = require('path');
const app = express();
const errorHandler = require('./errorHandler.cjs');
const createDirectoryAndFiles = require('./routes/createDirectoryAndFiles.cjs');

//Body parser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/files', createDirectoryAndFiles);

app.use('*', (req, res) => {
    return res.status(404).json({
        message: "Route Not Found!!",
        code: 404
    })
})

app.use(errorHandler);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});
